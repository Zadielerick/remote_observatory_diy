#include <string>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>
#include <sys/time.h>

struct timeval tv2;
unsigned long prev_time_usecs2 = 0;
unsigned long cur_time_usecs2, delta_usecs2;
float fps2;

using namespace std;
using namespace cv;

int main(int argc, char* argv[])
{
    string outText;
    string imPath = argv[1];

    // Create Tesseract object
    tesseract::TessBaseAPI *ocr = new tesseract::TessBaseAPI();

    // Initialize tesseract to use English (eng) and the LSTM OCR engine.
    //ocr->Init(NULL, "eng", tesseract::OEM_LSTM_ONLY);
    ocr->Init(NULL, "eng", tesseract::OEM_TESSERACT_ONLY);

    // Set Page segmentation mode to PSM_AUTO (3)
    ocr->SetPageSegMode(tesseract::PSM_AUTO);

    // Open input image using OpenCV
    Mat im = cv::imread(imPath, IMREAD_COLOR);

    // Set image data
    ocr->SetImage(im.data, im.cols, im.rows, 3, im.step);

	//Start Timer
	gettimeofday(&tv2, NULL);
	prev_time_usecs2 = 1000000 * tv2.tv_sec + tv2.tv_usec;

    // Run Tesseract OCR on image
    outText = string(ocr->GetUTF8Text());

	//Stop Timer and calculate delay
	gettimeofday(&tv2, NULL);
	cur_time_usecs2 = 1000000 * tv2.tv_sec + tv2.tv_usec;
	delta_usecs2 = cur_time_usecs2 - prev_time_usecs2;
	fps2 = 100.0f/((float)delta_usecs2/1000000.0f);
	prev_time_usecs2 = cur_time_usecs2;

	printf("%dms for CPU computation\n", delta_usecs2/1000);

    // print recognized text
    cout << outText << endl; // Destroy used object and release memory ocr->End();

    return EXIT_SUCCESS;
}
